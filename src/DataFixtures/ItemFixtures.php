<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 08/05/2020
 * Time: 14:44
 */

namespace App\DataFixtures;


use App\Entity\Item;
use Doctrine\Persistence\ObjectManager;

class ItemFixtures extends BaseFixtures
{
    private static $itemName = [
        'shop',
        'homework',
        'clean house',
        'sport'
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Item::class, 2, function (Item $item, $count) {
            $item->setName($this->faker->randomElement(self::$itemName));
            $item->setContent($this->faker->realText(1000, 2));
            $item->getCreatedAt($this->faker->dateTimeBetween('-30 minutes', '-1 seconds'));

        });

        $manager->flush();
    }


}