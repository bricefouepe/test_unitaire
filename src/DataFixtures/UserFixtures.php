<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends BaseFixtures
{
    private static $userFirstName = [
        'Brice',
        'Duy Dung',
        'Christian'
    ];

    private static $userLastName = [
        'Fouepe',
        'Tran',
        'Charpentier'
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(User::class, 10, function (User $user, $count) {
            $user->setFirstName($this->faker->randomElement(self::$userFirstName));
            $user->setLastName($this->faker->randomElement(self::$userLastName));
            $user->setPassword('password');
            $user->setBirthday($this->faker->numberBetween(15, 25));
            $user->setEmail('test' . $count . '@test.com');

        });

        $manager->flush();
    }
}
