<?php
/**
 * Created by PhpStorm.
 * User: bricefouepe
 * Date: 09/05/2020
 * Time: 17:12
 */

namespace App\Service;


use App\Entity\Item;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class EmailService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(Item $item)
    {
        if ($item->getTodoList()->getUser()->getBirthday() > 18) {
            return true;
        } else {
            return false;
        }
//        $email = (new Email())
//            ->from(new Address('bricefouepe@test.com'))
//            ->to(new Address($item->getTodoList()->getUser()->getEmail()) )
//            ->subject('test')
//            ->text('Lorem Ipsum');
//
//        $this->mailer->send($email);
//
//        return $email;
    }
}