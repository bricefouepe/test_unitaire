<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\TodoList;
use App\Entity\User;

class ToDoListService {

    private $user;

    public const MAX_ITEM_IN_TODO_LIST = 10;
    public const TIME_BETWEEN_TWO_ITEMS = 30;
    public const MAX_CONTENT = 1000;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function createTodolist() {

        if( !$this->user->isValid() ){
            throw new \RuntimeException("Your user must be valid");
        }

        if( $this->user->getTodoList() instanceof TodoList){
            throw new \RuntimeException("Todolist already exists");
        }
    }

    public function addItem(Item $item) {

        $todoList = $this->user->getTodolist();

        if(!$todoList instanceof TodoList){
            throw new \RuntimeException("User doesn't have a todolist");
        }

        if($todoList->getItems()->count() >= self::MAX_ITEM_IN_TODO_LIST){
            throw new \RuntimeException('Todolist is full (Maximum of 10 values)');
        }

        $last = $todoList->getItems()->last();
        if($last instanceof Item && floor(((new \DateTime)->getTimestamp() - $last->getCreatedAt()->getTimestamp()) / 60 ) < self::TIME_BETWEEN_TWO_ITEMS){
            throw new \RuntimeException('Time added too fast. You have to wait '.self::TIME_BETWEEN_TWO_ITEMS. ' minutes to add the next value');
        }

        if($item->getContent() === null || strlen($item->getContent()) === 0){
            throw new \RuntimeException('Content is empty');
        }

        if(strlen($item->getContent()) > self::MAX_CONTENT){
            throw new \RuntimeException('Content limit is 1000 characters');
        }

        foreach ($this->getTodolist()->getItems() as $i){
            if($i->getName() === $item->getName()){
                throw new \RuntimeException('Name not unique');
            }
        }

        if($todoList->canAddItem($item)){
            $todoList->addItem($item);
            return true;
        }

        return false;
    }

    public function getTodolist() {
        return $this->user->getTodoList();
    }
}