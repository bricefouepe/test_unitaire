<?php

namespace App\Tests\src\Entity;


use App\Entity\User;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
        $this->user->setFirstName('brice');
        $this->user->setLastName('brico');
        $this->user->setEmail('test@test.com');
        $this->user->setPassword('password');
        $this->user->setBirthday(Carbon::now()->subDecades(3)->subMonths(7)->subDays(24)->toDateString());
    }

    public function testIsValidName()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testIsNotValidEmailBadFormat()
    {
        $this->user->setEmail('test');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidFirstNameEmpty()
    {
        $this->user->setFirstName('');
        $this->assertFalse($this->user->isValid());
    }

//    public function testIsNotValidLastNameNull()
//    {
//       $this->user->setLastName(null);
//        $this->assertFalse($this->user->isValid());
//    }

    public function testIsNotValidMinor()
    {
        $this->user->setBirthday(Carbon::now()->subDecade()->toDateString());
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidPasswordToShort()
    {
        $this->user->setPassword('test');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidPasswordToLong()
    {
        $this->user->setPassword(random_bytes(45));
        $this->assertFalse($this->user->isValid());
    }
}