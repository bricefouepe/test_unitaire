<?php

namespace App\Tests;

use App\Entity\Item;
use App\Entity\TodoList;
use App\Entity\User;
use App\Service\EmailService;
use App\Service\ToDoListService;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    public function setUp() {

        $user = new User();
        $todolist = new TodoList();

        $user->setTodolist($todolist);
        $todolist->setUser($user);

        return new ToDoListService($user);
    }

    public function testAddItem() {
        $todolistService = $this->setUp();
        $item = new Item('name', 'abc');

        $this->assertTrue( $todolistService->addItem($item) );
    }

    public function testTodolistMissing() {
        $todolistService = $this->setUp();
        $todolistService->getTodolist()->getUser()->setTodolist(null);
        $item = new Item('name', 'abc');

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("User doesn't have a todolist");
        $todolistService->addItem($item);
    }

    public function testTodolistFull() {
        $todolistService = $this->setUp();
        $item = new Item('name', 'abc');

        for ($i = 0; $i < ToDoListService::MAX_ITEM_IN_TODO_LIST; $i++){
            $item = new Item('name'.$i, 'abc'.$i);
            $todolistService->getTodolist()->addItem($item);
        }

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Todolist is full (Maximum of 10 values)');
        $todolistService->addItem($item);
    }

    public function testTime() {
        $todolistService = $this->setUp();
        $item = new Item('name1', 'abc1');

        $lastItem = new Item('name2', 'abc2');
        $lastItem->setCreatedAt( (new \DateTime())->modify('-10 mins') );
        $todolistService->getTodolist()->addItem($lastItem);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Time added too fast. You have to wait '.ToDoListService::TIME_BETWEEN_TWO_ITEMS.' minutes to add the next value');
        $todolistService->addItem($item);
    }

    public function testContentNull() {
        $todolistService = $this->setUp();
        $item = new Item('name', null);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Content is empty');
        $todolistService->addItem($item);
    }

    public function testContentLength() {
        $todolistService = $this->setUp();

        $content = str_repeat('a', ToDoListService::MAX_CONTENT + 1);
        $item = new Item('name', $content);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Content limit is 1000 characters');
        $todolistService->addItem($item);
    }

    public function testNameNotUnique() {
        $todolistService = $this->setUp();
        $item1 = new Item('hihi', 'hihi');
        $item1->setCreatedAt( (new \DateTime())->modify('-30 mins') );

        $item2 = new Item('hihi', 'hihihi');
        $todolistService->addItem($item1);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Name not unique');
        $todolistService->addItem($item2);
    }

}