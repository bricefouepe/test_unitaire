<?php

namespace App\Tests;

use App\Entity\TodoList;
use App\Entity\User;
use App\Service\ToDoListService;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class ToDoListTest extends TestCase
{
    public function setUp() {

        $user = new User();

        $user->setFirstName('TRAN');
        $user->setLastName('Dzung');
        $user->setEmail('yoyoyo@gmail.com');
        $user->setBirthday(Carbon::now()->subDecades(3)->subMonths(7)->subDays(24)->toDateString());
        $user->setPassword('abcdqsdqdqdaze');

        $todolist = new TodoList();
        $user->setTodolist($todolist);
        $todolist->setUser($user);

        return $user;
    }


    public function testAddTodolist() {
        $user = $this->setUp();
        $this->assertTrue( $user->getTodolist() instanceof TodoList );
    }

//    public function testUserNotValid() {
//        $user = $this->setUp();
//        $user->setEmail('yoyoyo@gmail.com');
//
//        $this->expectException(\RuntimeException::class);
//        $this->expectExceptionMessage('You need to create a new ToDolist');
//
//        (new ToDoListService($user))->createTodolist();
//    }

    public function testTodolistAlreadyExist() {
        $user = $this->setUp();

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Todolist already exists');

        (new ToDoListService($user))->createTodolist();
    }
}