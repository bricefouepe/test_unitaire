<?php

namespace App\Tests\Service;

use App\Entity\Item;
use App\Entity\TodoList;
use App\Entity\User;
use App\Service\EmailService;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;

class MailerTest extends TestCase
{
    private $item;

    private $user;

    private $todoList;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
        $this->user->setFirstName('brice');
        $this->user->setLastName('brico');
        $this->user->setEmail('test@test.com');
        $this->user->setPassword('password');
        $this->user->setBirthday(Carbon::now()->subDecades(3)->subMonths(7)->subDays(24)->toDateString());

        $this->todoList = new TodoList();
        $this->todoList->setUser($this->user);

        $this->item = new Item();
        $this->item->setName('brice');
        $this->item->setContent('test content');
        $this->item->setTodoList($this->todoList);
    }

    public function testSendEmail()
    {

        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())
            ->method('send');

        $mailer = new EmailService($symfonyMailer);
        $email = $mailer->send($this->item);

        $this->assertTrue($email);


//        $this->assertSame('test', $email->getSubject());
//        $this->assertCount(1, $email->getTo());
//
//        /** @var Address[] $address */
//        $address = $email->getTo();
//        $this->assertInstanceOf(Address::class, $address[0]);
//        $this->assertSame('Brice', $address[0]->getName());
//        $this->assertSame($item->getTodoList()->getUser()->getEmail(), $address[0]->getAdress());
    }
}
